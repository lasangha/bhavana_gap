#Bhavana
Retiro personal de meditación
Version 0.3.x

Un retiro personal de meditación.

Meditaciones guiadas, ayuda y más.

Construido gracias a Drupal, jDrupal, Phonegap, jQuery, jQuery Mobile, PHP, MariaDB y muchos otros más.

#Licencia
MIT

Revisar las licencias de cada uno de los componentes para más detalles.

#Pruebas
http://meditacion.lasangha.org
