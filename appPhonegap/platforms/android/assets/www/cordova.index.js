var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        //document.addEventListener('deviceready', Cordova_boot, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('The device is ready to rumble!');
        Cordova_boot();
        // Testing notifications
        //app.testNotification();
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        console.log('Received Event: ' + id);
    },
};

/**
 * This is what you should call from your pages, call it at the bottom
 */
function Cordova_boot(){

    var d = new Date(0);
    d.setUTCSeconds(timestamp);
    timestamp = d.toString();

    console.log("Booting from cordova ?" + timestamp);
    console.log("Booting from cordova again");
    //Cala_runMe.push(Cala_menuParse);
    //Cala_runMe.push(Cala_loadThisPath);
    //Cala_initMe();
}

// Directly init the app
app.initialize();

