/*******************************************************************************
 *
 * Cala
 *
 */

Cala.frontPage = "bhavana/default";

// Change to true to run on an app
Cala.onApp = false;

// What device is this? Android|iOs|Comp
// Set the correct option for internal storage and other minor aspects
Cala.Device = 'comp';

/*******************************************************************************
 *
 * jDrupal
 *
 */
// Set the site path (without the trailing slash). 
Drupal.settings.site_path = "http://192.168.43.164/dirz"; //http://www.example.com
//Drupal.settings.site_path = "http://127.0.0.1/dirz"; //http://www.example.com

// Set the Service Resource endpoint path.
Drupal.settings.endpoint = "rest_0";
