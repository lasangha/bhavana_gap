<?php

// TMP while I move this here
function _geoloc_getMeVisitorDetails(){
    return array("longitude" => 0, "latitude" => 0);
}

/**
 * Registers meditations times
 */
function bhavana_causesGetMaxTime(){

	$q = "SELECT idCause, name, totalTime FROM {bhavana_causes} WHERE idCause > 1 ORDER BY totalTime DESC LIMIT 1";

	$maxCause = db_query($q)->fetchAssoc();

	return $maxCause;

}

/**
 * Registers meditations times
 */
function bhavana_meditationAddTime($totalTime, $causeCode, $privacy, $where, $ref) {

    global $user;

    # Load geolocator
    //modules_loader('geoloc');

    # I will only load the cause code if required aka not 'non'

    # Get the correct id of the cause
    # Standard db_query drupal way was not working, but today, nothing is working much
    $idCause = db_query("SELECT idCause FROM {bhavana_causes} WHERE code = '".$causeCode."'")->fetchField();

    # Privacy respect
    if($privacy == 0){
        # Where is this person?
        $location = _geoloc_getMeVisitorDetails();
    }else{
        $location = array("longitude" => 0, "latitude" => 0);
    }

    if($idCause > 0){
        # Register the new time
        $q = sprintf("INSERT INTO {bhavana_meditations} (`timestamp`, `totalTime`, `idCause`, `where`, `idUser`, `coordinates`, `ref`)
            VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')",
                time(),
                $totalTime,
                $idCause,
                $where,
                $user->uid,
                $location['longitude'] . "," . $location['latitude'],
                $ref
            );
        try{
            $r = db_query($q);
        }catch(Exception $e){
            //echo $e;
        }
    }

    # Update total times
    $q = sprintf("UPDATE {bhavana_causes} SET totalTime = totalTime + %s WHERE idCause = '%s'", $totalTime, $idCause);
    $r = db_query($q);

    # I will just say it worked, not much to do if it did not.
    return 'SUCCESS_ALL_GOOD';

}

/**
 * Get group meditations times per day
 */
function bhavana_meditationsGetGroupMeditations($ini = 7){

    $q = sprintf("
        SELECT FROM_UNIXTIME(m.timestamp, '%%Y.%%e.%%m') AS day, SUM(m.totalTime) AS totalTime, m.idCause, c.name AS cName, c.code AS cCode
        FROM {bhavana_meditations} m
        INNER JOIN `bhavana_causes` c ON c.idCause = m.idCause
        WHERE timestamp > %s
        GROUP BY idCause, day
        ORDER BY day
        ", (time()-($ini*86400)));

    $r = db_query($q);

    $stuff = array();
    $details = array();
    $dates  = array();

    foreach($r as $row){
        $stuff[$row->cCode][] = array("name" => $row->cName, "day" => $row->day, "totalTime" => $row->totalTime);
        $details[$row->cName]['details'][$row->day] = $row->totalTime;
        $dates[] = $row->day;
    }

    # Remove duplicates
    $dates = array_unique($dates);

    # Lets fix the dates (remove keys)
    $nDates = array();
    foreach($dates as $d){
        $nDates[] = $d;
    }

    # Fix dates to see if there are any missing ones?
    foreach($details as $topic => $d){
        foreach($dates as $date){
            if(!array_key_exists($date, $details[$topic]['details'])){
                $details[$topic]['details'][$date] = 0;
            }
        }
				# Sort them out
        ksort($details[$topic]['details']);
    }

    # Now, lets get rid of all that is not needed
    $justTheNumbers = array();
    foreach($details as $topic => $d){
        foreach($dates as $date){
            $justTheNumbers[$topic][] = $details[$topic]['details'][$date];
        }
    }

    return array('details' => $justTheNumbers, 'dates' => $nDates);

}

/**
 * Get the medatation locations in order to create a map
 */
function bhavana_meditationsGetLocations(){

    $q = "SELECT coordinates, timestamp
        FROM `bhavana_meditations`
        WHERE timestamp > " . (time() - 1296000) . "
        AND coordinates != ''
        AND coordinates != ',' 
        GROUP BY coordinates
        ORDER BY timestamp DESC
        LIMIT 0, 60";

    $res = db_query($q);

    $dats = array();
    foreach($res as $record){
        $dats[] = sprintf('{"type":"Feature","id":"%s","properties":{"name":"90"},"geometry":{"type":"Point","coordinates":[%s]}}',
            $record->timestamp,
            $record->coordinates);
    }

    $dats = implode(",", $dats);

    return '{"type":"FeatureCollection","features":['.$dats.']}';

}

/**
 * Retrieves the meditation times, per day, for this person
 */
function bhavana_meditationsGetMyTimes($ini){

    global $user;

    $q = sprintf("
        SELECT FROM_UNIXTIME(timestamp, '%%Y.%%e.%%m') AS day, SUM(totalTime) AS totalTime
        FROM {bhavana_meditations}
        WHERE idUser = '%s'
        AND timestamp > %s
        GROUP BY day
        ORDER BY day
        ", $user->uid, (time()-($ini*86400)));

    $res = db_query($q);
    $labels = array();
    $times = array();
    if($res > 0){
        foreach($res as $r){
            $labels[] = $r->day;
            $times[] = $r->totalTime;
        }
    }
    return array("labels" => $labels, "times" => $times);

}

/**
 * @brief Gets my meditation times grouped by cause
 *
 * @return Required information to create a pie chart with chart.js
 **/
function bhavana_meditationsGetMyTimesPerCause(){

    global $user;

    $q = sprintf("
        SELECT SUM(m.totalTime) AS totalTime,
            c.name AS causeName, c.code AS causeCode
            FROM {bhavana_meditations} m
            INNER JOIN {bhavana_causes} c on c.idCause = m.idCause
            WHERE idUser = %s
            GROUP BY m.idCause
            ", $user->uid);

    $results = db_query($q);

    $data = array();

    # Colours
    $colours['non'] = "#E74D1E";
    $colours['paz'] = "#E4DF4D";
    $colours['com'] = "#4DE45A";
    $colours['hum'] = "#4DC1E4";

    foreach($results as $row){
        $data[] = array(
            'value' => $row->totalTime,
            'label' => $row->causeName,
            'color' => $colours[$row->causeCode]
        );
    }

    return $data;

}
