<?php

function bhavana_services_resources() {
    $resources = array(
        'bhavana_resources' => array(
            'actions' => array(
                'meditation_add_times' => array(
                    'help' => t('Adds meditations sessions times.'),
                    'file' => array(
                        'type' => 'inc',
                        'module' => 'bhavana',
                        'name' => 'bhavana.resource',
                    ),
                    'callback' => 'bhavana_meditationAddTime',
                    'args' => array(
                        array(
                            'name'         => 'totalTime',
                            'type'         => 'text',
                            'description'  => t('Meditated Time'),
                            'source'       => array('data' => 'totalTime'),
                            'optional'     => FALSE,
                        ),
                        array(
                            'name'         => 'causeCode',
                            'type'         => 'char',
                            'description'  => t('Code of the cause'),
                            'source'       => array('data' => 'causeCode'),
                            'optional'     => FALSE,
                        ),
                        array(
                            'name'         => 'privacy',
                            'type'         => 'int',
                            'description'  => t('Privacy setting'),
                            'source'       => array('data' => 'privacy'),
                            'optional'     => FALSE,
                        ),
                        array(
                            'name'         => 'where',
                            'type'         => 'text',
                            'description'  => t('Where did the medatation take place'),
                            'source'       => array('data' => 'where'),
                            'optional'     => FALSE,
                        ),
                        array(
                            'name'         => 'ref',
                            'type'         => 'text',
                            'description'  => t('Reference like timer or a youtube video'),
                            'source'       => array('data' => 'ref'),
                            'optional'     => FALSE,
                        )),

                    'access callback' => 'user_access',
                    'access arguments' => array('register meditation times'),
                    'access arguments append' => false,
                ), // /meditation_add_times
                'causes_get_max_time' => array(
                    'help' => t('Gets the meditation with max times set'),
                    'file' => array(
                        'type' => 'inc',
                        'module' => 'bhavana',
                        'name' => 'bhavana.resource',
                    ),
                    'callback' => 'bhavana_causesGetMaxTime',
                    'args' => array(),
                    'access callback' => 'user_access',
                    'access arguments' => array('access content'),
                    'access arguments append' => false,
                ), //causes_get_max_time
                'meditation_get_group_times' => array(
                    'help' => t('Gets the meditation times for everybody'),
                    'file' => array(
                        'type' => 'inc',
                        'module' => 'bhavana',
                        'name' => 'bhavana.resource',
                    ),
                    'callback' => 'bhavana_meditationsGetGroupMeditations',
                    'args' => array(
                        array(
                            'name'         => 'ini',
                            'type'         => 'int',
                            'description'  => t('When should I start?'),
                            'source'       => array('data' => 'ini'),
                            'optional'     => true,
                        )),
                    'access callback' => 'user_access',
                    'access arguments' => array('access content'),
                    'access arguments append' => false,
                ), //meditation_get_group_times
                'meditation_get_map' => array(
                    'help' => t('Gets the list of locations for the meditation to put on a map'),
                    'file' => array(
                        'type' => 'inc',
                        'module' => 'bhavana',
                        'name' => 'bhavana.resource',
                    ),
                    'callback' => 'bhavana_meditationsGetLocations',
                    'args' => array(
                        array(
                            'name'         => 'ini',
                            'type'         => 'int',
                            'description'  => t('When should I start?'),
                            'source'       => array('data' => 'ini'),
                            'optional'     => true,
                        )),
                    'access callback' => 'user_access',
                    'access arguments' => array('access content'),
                    'access arguments append' => false,
                ), //meditation_get_map
                'meditation_get_my_times_per_day' => array(
                    'help' => t('Gets my meditations times per day'),
                    'file' => array(
                        'type' => 'inc',
                        'module' => 'bhavana',
                        'name' => 'bhavana.resource',
                    ),
                    'callback' => 'bhavana_meditationsGetMyTimes',
                    'args' => array(
                        array(
                            'name'         => 'ini',
                            'type'         => 'int',
                            'description'  => t('When should I start?'),
                            'source'       => array('data' => 'ini'),
                            'optional'     => true,
                        )),
                    'access callback' => 'user_access',
                    'access arguments' => array('access content'),
                    'access arguments append' => false,
                ), //meditation_get_my_times_per_day
                'meditation_get_my_times_per_cause' => array(
                    'help' => t('Gets my meditations times per cause'),
                    'file' => array(
                        'type' => 'inc',
                        'module' => 'bhavana',
                        'name' => 'bhavana.resource',
                    ),
                    'callback' => 'bhavana_meditationsGetMyTimesPerCause',
                    'args' => array(),
                    'access callback' => 'user_access',
                    'access arguments' => array('access content'),
                    'access arguments append' => false,
                ), //meditation_get_my_times_per_cause
            ),
        ),
    );
    return $resources;
}
