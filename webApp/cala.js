/****************************************************************************
/*!
 * Cala Framework: To make your life simpler
 * @requires jQuery v1.5? or later (maybe other things too)
 *
 * Copyright (c) 2015 Twisted Head
 * License: MIT

 * Include this AT THE BOTTOM of your pages, that is all you need to do.

 <script>Cala.boot();</script>


 *           | |      
 *   ___ __ _| | __ _ 
 *  / __/ _` | |/ _` |
 * | (_| (_| | | (_| |
 *  \___\__,_|_|\__,_|

 *****************************************************************************/                   
// Version
var version = "0.1.3-dev";

//! The main instance of Cala
var Cala = Cala || {};

//! Some constants
var ERROR_NO_VALID_USER         = "-300";
var ERROR_USER_WRONG_LOGIN_INFO = "-301";
var ERROR_USER_NO_VALID_SESSION = "-302";
var ERROR_USER_ACCESS_DENIED    = "-303";
var ERROR_USER_EXISTS           = "-304";
var ERROR_BAD_REQUEST           = "-1";
var ERROR_NO_REQUEST_DONE       = "-900000";
//Database
var ERROR_DB_NO_RESULTS_FOUND   = "-200";

//! Internal useful things
VAR_GO_NOT_LOGGED_IN = '?x=login';

//! Some variables
//var Cala_tplPath = "tpl/";

// Where is the api?
//var Cala_apiUrl = "";

// Basic variables needed to exist
//! The main front page
var Cala_frontPage = 'index.html';

//Where do you want me to go upon logout?
//var Cala_goOnOut = 'index.html';

//Some needed stuff

// Some other constants
// Parameters sent via url
var params = false;

/**
 * Run stuff during boot up, if you want me to run stuff, let me know
 */
var Cala_runMe = [];

// Main Cala instance
//var Cala = {};

/*****************************************************************************/
//
// Boot stuff
//
//////////////////////////////////////////////////////////////////////////////

/**
 * This is where it all begins, this should be run at the very beggining 
 * of the page load, or when document is ready, but you never call this directly,
 * you need to call whereAmI();
 */
function Cala_initMe(){

    Cala_say("Booting up the car!");

    // Perform a System Connect call.
    // @todo This should be core for the system to run
    system_connect({
        success:function(result){
            if (Drupal.user.uid === 0) {
                message = "Hello World, Visitor!";
            }else {
                message = "Hello World, " + Drupal.user.name + "!";
            }

    Cala_say("Hello: " + message);

    // Get the params
    params = Cala_getUrlVars();

    // Run things that people want me to run
    for(i = 0; i < Cala_runMe.length; i++){
        Cala_say("running..." + i);
        Cala_justRunThis(Cala_runMe[i]);
    }
        },
            error:function(xhr, status, message) {
                // Go To The Error Page
                console.log("Error on system call, ther could be a problem with the internet or the main server");
                iGoTo('errorNoConn');
            }
    });

}

// Actually run functions
function Cala_justRunThis(what){
    what();
}

/**
 * This is what you should call from your pages, call it at the bottom
 */
function Cala_boot(){

    Cala_say("I am on a computer");
    Cala_runMe.push(Cala_loadThisPath);
    Cala_runMe.push(Cala_menuParse);
    $(document).ready(Cala_initMe);

}

//! Sets code to be run when the page is ready
function Cala_runOnReady(runMe){
    $(document).ready(runMe);
}

// 'Fix' the top menu with additional links and put the users name on it
function Cala_menuParse(){

    Cala_say("Am I logged in? Should I change the menu?");

    if (Drupal.user.uid > 0) {

        $("#logMeIn").text("");

        $("#userMenuTitle").html(Drupal.user.name);

        newLink = '' +
            '<li class="divider"></li>' +
            '<a href="?x=myAccount" class="list-group-item">' +
            '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>' +
            ' Configuración</a>' +
            '<a href="#" onClick="return Cala_logMeOut();" class="list-group-item">' +
            '<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>' +
            ' Salir</a>';

        $("#userMenu").append(newLink);

    }else{
        Cala_say("No, not logged in at all");
    }

}

/***************************************************************************
 *
 * Local storage
 *
 */

//! Store keys in local storage
function keyStore(key, value){
    Cala_say("Storing key: " + key);
    window.localStorage.setItem(key, value);
    return true;
}

//! Get key from local storage
function keyGet(key, defaultValue){
    var value = window.localStorage.getItem(key);
    if(value === null){
        Cala_say("No value found, I will use the default");
        value = defaultValue;
    }
    Cala_say("Gotten Key: " + key + " with value: " + value);
    return value;
}

//! Remove key from local storage
function removeKey(theKey){

    Cala_say("Removing key: " + theKey);
    // Remove them all
    if(theKey === ''){

    }else{
        window.localStorage.removeItem(theKey);
    }
}

/**
 * Remove keys after logout
 */
function _logMeOutRmKeys(){

    Tpl_msgSuccess("Hasta luego :)");

    keysGone = [VAR_CURRENT_USER_NAME, VAR_CURRENT_SESSION_KEY];

    // If I don't do it like this, it won't be able to remove them all
    for(i = 0; i < keysGone.length; i++){
        removeKey(keysGone[i]);
    }

    iGoTo('index.html');

}

/*****************************************************************************/
//
// Messaging and alerts
//
//////////////////////////////////////////////////////////////////////////////
Cala.Messages = {

    //! Clear the alert messages
    clear: function(){
        $("#Cala_alertMessages").hide('slow', function(){
            $("#Cala_alertMessages").html('');
        });
        return this;
    },

    /**
     * Helper function to actually present messages to the user, you should never
     * call this directly
     */
    _alert: function(what, type){

        console.log("Internal message");

        $("#Cala_alertMessages").show('slow', function(){

            $("#Cala_alertMessages").html('' +
                '<div class="alert alert-'+type+' alert-dismissible fade in" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>' +
                what +
                '</div>'
                );
        });
        return this;
    },

    //! Alert messages to the user
    danger: function(what){
        this._alert(what, 'danger');
        return this;
    },

    // Alert warning messages to the user
    warning: function(what){
        this._alert(what, 'warning');
        return this;
    },

    // Alert information messages to the user
    info: function(what){
        this._alert(what, 'info');
        return this;
    },

    //! Success messages
    success: function(what){
        this._alert(what, 'success');
        return this;
    }
};

/*****************************************************************************/
//
// Other tools
//
//////////////////////////////////////////////////////////////////////////////

//! Add a css
function Tpl_addCss(which){

    // Is this an external css
    if(strpos(which, 'http', 0) === false){
        Cala_say("Adding an internal css file");
        which = "tpl/" + which;
    }

    Cala_say("Adding a css" + which);
    $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', which) );
}

/**
 *  I parse a tpl and replace it with some values
 *  @param tpl The template
 *  @param values An object with the values to replace
 *  @param bl Do you want me to replace the breaklines (\n) with <br />
 */
function parseTpl(tpl, values, bl){

    // Loop each value
    for (var key in values) {
        var theKey = "{" + key + "}";
        var re = new RegExp(theKey, "g");
        tpl = tpl.replace(re, values[key]);
    }

    // Should I add html breakLines?
    if(bl === true){
        tpl = textAddBreakLines(tpl);
    }

    return tpl;

}

// Parse a date from a UTC timestamp
// Read more https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Text_formatting
function dateParse(timestamp){

    //var utcSeconds = 1234567890;
    var d = new Date(0);
    d.setUTCSeconds(timestamp);
    timestamp = d.toString();

    return timestamp;

}

/**
 * I will redirect somewhere
 * @deprecated use Cala.iGoTo(goTo);
 */
function iGoTo(goTo){
    Cala_say("Going to: " + goTo);
    window.location.href = goTo;
}

/**
 * String poss of a word/string in a string
 * http://phpjs.org/functions/strpos/
 * @param haystack where are you looking in?
 * @param needle what are you looking for?
 * @param offset where do you want to start the search?
 */
function strpos(haystack, needle, offset) {
    //  discuss at: http://phpjs.org/functions/strpos/
    // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Onno Marsman
    // improved by: Brett Zamir (http://brett-zamir.me)
    // bugfixed by: Daniel Esteban
    //   example 1: strpos('Kevin van Zonneveld', 'e', 5);
    //   returns 1: 14

    var i = (haystack + '')
        .indexOf(needle, (offset || 0));
    return i === -1 ? false : i;
}

/**
 * Parse url parameters
 * http://jquery-howto.blogspot.com/2009/09/get-url-parameters-values-with-jquery.html
 */
function Cala_getUrlVars(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
        Cala_say("Found param: " + hash[0] + hash[1]);
    }
    return vars;
}

// Get a param coming from the url or a default value
function Cala_paramsGet(which, def){
    // TMP solution, I will move all params in here if I can
    params = window.params;
    if(params[which] !== undefined && params[which] !== ''){
        Cala_say("Found param with value" + params[which]);
        return params[which];
    }
    else{
        Cala_say("No custom path");
        return def;
    }	
}

// Add html break lines to the text
function textAddBreakLines(text){
    var theKey = "\n";
    var re = new RegExp(theKey, "g");
    text = text.replace(re, "<br />");
    return text;
}

//! Get the correct page for this path
function Cala_loadThisPath(){
    Cala_say("Loading the current path: " + params.x);
    path = Cala_paramsGet('x', Cala.frontPage);
    $("#mainContent").load("modules/" + path + ".html?ppp=" + Math.floor(Math.random() * 1000));
}

//! Say something
function Cala_say(what){
    console.log(what);
}

/**
 * Check to see if the user is logged in, this will be extended to use all the
 * Drupal perms
 * It does not really work at the moment
 * @param redirect If you want to send the person somewhere, let me know
 */ 
function users_loggedInRequired(redirect){
    if(keyGet(VAR_CURRENT_USER_NAME, '') === ''){
        if(redirect !== ''){
            iGoTo(redirect);
        }else{
            return false;
        }
    }
    return true;
}

/**
 * Opens external links in browser or in an external brower if running on phonegap
 */
function Cala_externalLink(linkId){

    // I will only act if I am running on an app
    if(Cala.onApp === false){
        return true;
    }else{
        // Get the path
        var whereTo = $("#" + linkId).attr("href");
        console.log("Clicked me: " + whereTo);
        navigator.app.loadUrl(whereTo, { openExternal:true });
        return false; 
    }
}

/*****************************************************************************/
//
// User Management
//
//////////////////////////////////////////////////////////////////////////////
/**
 * Log a user in
 */
function Cala_logMeIn(){

    userName = $("#myUserName").val();
    password = $("#myPwd").val();

    if(userName === "" || password  === ""){
        console.log("Something is missing");
        Cala.Messages.clear().warning("Faltan algunos campos");
        return false;
    }

    _Cala_logMeIn(userName, password);

    return false;
}

/**
 * Helper function to actually log someone in
 */
function _Cala_logMeIn(userName, password){
    user_login(userName, password, {
        success:function(result){
            alert('Hola ' + result.user.name + '!');
            iGoTo("index.html");
        },
    error:function(xhr, status, message){
        Cala.Messages.clear().warning("Datos incorrectos, ¿Quizá olvidó su clave?");
        console.log("Unable to login");
    }
    });
}

/**
 * Log a user in
 */
function Cala_userRegister(){

    userEmail = $("#myEmailR").val();
    password = $("#myPwdR").val();

    if(userEmail === "" || password  === ""){
        console.log("Something is missing");
        Cala.Messages.clear().warning("Faltan algunos campos");
        return false;
    }

    var account = {
        name: userEmail,
        mail: userEmail,
        pass: password
    };

    user_register(account, {
        success:function(result) {
            console.log('Registered user #' + result.uid);
            _Cala_logMeIn(userEmail, password);
        },
        error: function(result){
            Cala.Messages.clear().warning("Hubo un error, quizá ya este correo existe, si este es su correo y olvidó la clave, puede solicitar una nueva.");
        }
    });

    return false;

}

/**
 * Log a user out
 */
function Cala_logMeOut(){
    user_logout({
        success:function(result){
            if (result[0]) {
                alert("Hasta luego!");
                iGoTo("index.html");
            }
        }
    });
}

/**
 * Request a new password
 */
function Cala_usersPasswordReset(){

    userName = $("#myRecoverEmail").val();

    if(userName === ""){
        console.log("Something is missing");
        Cala.Messages.clear().warning("Requiero al menos un usuario para este procedimiento");
        return false;
    }

    user_request_new_password(userName, {
        success: function(result) {
            if (result[0]) {
                console.log("All good, a new password in underway");
                Cala.Messages.clear().success("Las instrucciones para terminar con el proceso serán enviadas a su correo electrónico");
            }
        },
        error: function(){
            Cala.Messages.clear().danger("Hubo un error, quizá el correo no existe. Favor intente de nuevo.");
        }
    });
    return false;
}

