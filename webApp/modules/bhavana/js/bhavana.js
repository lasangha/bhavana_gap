/****************************************************************************
 *
 * Navigation
 *
 */

function Bhavana_storeThisPage(){

	// Re set this page
	thisPage = document.URL;

	console.log("Storing this page: >>>>> " + thisPage);

	// Is this an storable page?
	if(Cala_paramsGet("x", "") != "bhavana/sessions"){
		console.log("I shall not store this page!");
		return false;
	}
	else{
		keyStore("var_bhavana_lastPage", thisPage);
	}
}

/****************************************************************************
 *
 * Meditations
 *
 */
// Add meditation times to the causes
function Bhavana_addToCause(_totalTime, _causeCode){

    console.log("I will submit this time to the causes?");

    // Lets see first if the user wants to participate in global meditations
    var privacyOption = keyGet("bhavana_privacyGlobalMeditations", 1);

    if(privacyOption == '0'){
        Cala_say("No, s/he does not want to participate");
    }

    var myData = {causeCode: _causeCode, totalTime: _totalTime, privacy: privacyOption, where: (Cala.onApp === true ? 'app' : 'web')};
    Drupal.services.call({
        method: 'POST',
        path: 'bhavana_resources/meditation_add_times.json',
        data: JSON.stringify(myData),
        success: function(result) {
            console.log("Result: " + result);
        }
    });

}

// I retrieve the medatitation with most meditated time
function bhavana_getMeditationMaxCauseTime(){

    console.log("I will get the cause with more minutes meditated...");

    console.log("Getting times");

    //Lets make the call
    Drupal.services.call({
        method: 'POST',
        path: 'bhavana_resources/causes_get_max_time.json',
        success: function(result) {
            console.log("Result: " + result);
            if(typeof result == 'object'){
                console.log("Got information about the times");
                $("#causesTotalMinutes").html(result.totalTime);
                $("#causesCause").html(result.name);
                console.log(result.totalTime);
            }
        },
        error: function(){
            console.log("Something wrong?");
        }
    });
}

/****************************************************************************
 *
 * Privacy Settings
 *
 */

// Stores the privacy settings for this person in this device
function Bhavana_privacyGlobalSet(){

    //Which setting was selected
    var privacyOption = $("#bhavana_privacyGlobalMeditations").val();
    Cala_say("User selected privacy option: " + privacyOption);
    keyStore("bhavana_privacyGlobalMeditations", privacyOption);
}

// Gets the privacy settings for this person in this device
function Bhavana_privacyGlobalGet(){

    //Which setting was selected
    var privacyOption = keyGet("bhavana_privacyGlobalMeditations", 1);
    $("#bhavana_privacyGlobalMeditations").val(privacyOption);
}

/****************************************************************************
 *
 * Sessions
 *
 */

// I will take you to the last visited page of the course
function Bhavana_gotoLastSessionPage(){

    // Where I'm I?
    var thisPage = document.URL;

    console.log("I am in page: " + thisPage);

    // Where was I?
    var lastValue = keyGet("var_bhavana_lastPage", "startMeOver");

    // If there is a last location, I'll go there
    if(lastValue === undefined || lastValue == "startMeOver"){
        Cala_say("Nothing set");
        iGoTo("?x=bhavana/c_intro");
    }
    else{
        Cala_say("Going to: " + lastValue);
        iGoTo(lastValue);
    }

    return false;

}

/****************************************************************************
 *
 * Reports and Charts
 *
 */

function Bhavana_createChart(chartTitle, canvasId, chartLabels, chartData, type){

    console.log("Creating chart");

    if(chartLabels.length === 0){
        console.log("No information");
        Bhavana_chartsNoInfoMsg("#Bhavana_chartTotalPerDayMeDiv");
    }else{

        console.log("There are some meditations: " + chartLabels.length);

        var lineChartData = {
            labels : chartLabels,
            datasets : [
            {
                label: chartTitle,
                fillColor: "rgba(76,153,0,0.2)",
                strokeColor: "rgba(76,153,0,1)",
                pointColor: "rgba(76,153,0,1)",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke: "rgba(76,153,0,1)",
                data : chartData
            }
            ]
        };

        var ctx = document.getElementById(canvasId).getContext("2d");
        window.myLine = new Chart(ctx).Line(lineChartData, {responsive: true});
    }
}

// Create a 'no results' message in charts
function Bhavana_chartsNoInfoMsg(id){
    console.log("No information on: " + id);
    $(id).html("<div role='alert' class='alert alert-warning'>No hay resultados en este momento</div>");
}

// Get all meditation times per day, for me?
function Bhavana_getAllMeditationTimesPerDay(){

    Cala_say("There is connexion, lets get the times");

    Drupal.services.call({
        method: 'POST',
        path: 'bhavana_resources/meditation_get_group_times.json',
        data: JSON.stringify({ini: 7}),
        success: function(resp) {
            if(resp.dates.length === 0){
                console.log("No results");
                $("#groupMeditationsBody").html("<div role='alert' class='alert alert-warning'>No hay resultados en este momento</div>");
            }else{
            var newData = {
                labels: resp.dates,
        datasets: [
    {
        label: "Solo Meditando",
        fillColor: "rgba(231,77,30,0.2)",
        strokeColor: "rgba(231,77,30,1)",
        pointColor: "rgba(231,77,30,1)",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(255,255,153,1)",
        data: resp.details.Ninguna
    },
        {
            label: "Paz",
        fillColor: "rgba(255,255,153,0.2)",
        strokeColor: "rgba(255,255,153,1)",
        pointColor: "rgba(255,255,153,1)",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(255,255,153,1)",
        data: resp.details.Paz
        },
        {
            label: "Humildad",
            fillColor: "rgba(51,255,153,0.2)",
            strokeColor: "rgba(51,255,153,1)",
            pointColor: "rgba(51,255,153,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(51,255,153,1)",
            data: resp.details.Humildad
        },
        {
            label: "Compasión",
            fillColor: "rgba(76,153,0,0.2)",
            strokeColor: "rgba(76,153,0,1)",
            pointColor: "rgba(76,153,0,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(76,153,0,1)",
            data: resp.details['Compasi\u00f3n']
        }
    ]
            };
            var ctx = document.getElementById("myChart").getContext("2d");
            var options = {
                responsive: true,
                multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",
                legendTemplate : "<ul class=\"list-group <%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li class=\"list-group-item\"><span style=\"background-color:<%=datasets[i].strokeColor%>\">&nbsp;&nbsp;</span>&nbsp;<%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
            };
            window.myGroupTimesChart = new Chart(ctx).Line(newData, options);
            document.getElementById("groupTimesChart").innerHTML = myGroupTimesChart.generateLegend();
        }
        }
    });

}

// Get my meditation times per day
function Bhavana_getMyMeditationTimesPerDay(){

    Cala_say("Lets get the times");

    Drupal.services.call({
        method: 'POST',
        path: 'bhavana_resources/meditation_get_my_times_per_day.json',
        data: JSON.stringify({ini: 7}),
        success: function(resp) {
            Bhavana_createChart("Meditación por día", "Bhavana_chartTotalPerDayMe", resp.labels, resp.times, 'lines');
        },
        error: function(){
            Bhavana_chartsNoInfo("");
                Cala_say("Nothing found");
            Cala_say("Error");
        }
    });

}

// Get my meditation times per cause
function Bhavana_getMyMeditationTimesPerCause(){

    Cala_say("Lets get the meditation chart per cause");

    Drupal.services.call({
        method: 'POST',
        path: 'bhavana_resources/meditation_get_my_times_per_cause.json',
        success: function (response) {
                Cala_say("Got good information for the pie");

                var options = {legendTemplate: "<ul class=\"list-group <%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li class=\"list-group-item\"><span style=\"background-color:<%=segments[i].fillColor%>\">&nbsp;<%=segments[i].value%>&nbsp;</span>&nbsp;<%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"};
                var ctx = document.getElementById('chartPiePerCause').getContext('2d');
                var perCausePieChart = new Chart(ctx).Pie(response, options);
                document.getElementById("Bhavana_meditationTimesPerCauseLegend").innerHTML = perCausePieChart.generateLegend();
        },
        error: function(){
            Cala_say("Error getting information about my times per cause");
        }
    });

}

function Bhavana_createMap(){

    console.log("creating the map");

    var styleFunction = function(feature, resolution) {
        style = [new ol.style.Style({
            image: new ol.style.Circle({
                radius: 25,
                fill: new ol.style.Fill({
                    color: 'rgba(255, 153, 0, 0.4)'
                }),
                stroke: new ol.style.Stroke({
                    color: 'rgba(255, 204, 0, 0.2)',
                width: 1
                })
            })
        })];
        return style;
    };

    var map = new ol.Map({
        target: 'myMap',
        layers: [
        new ol.layer.Tile({
            source: new ol.source.OSM()
        }),
            new ol.layer.Vector({
                source: new ol.source.GeoJSON({
                    projection: 'EPSG:3857',
                url: apiPath + '?what=getMeditationLocations'
                })
            })
    ],
        view: new ol.View({
            center: [0, 0],
        zoom: 1
        }),
    });
}



